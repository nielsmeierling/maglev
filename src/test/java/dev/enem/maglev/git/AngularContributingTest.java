package dev.enem.maglev.git;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

import java.util.Collection;

public class AngularContributingTest {

    String DEFAULT_ANGULAR_FORMAT = "<type>(<scope>): <subject>\n" +
                         "<BLANK LINE>\n" +
                         "<body>\n" +
                         "<BLANK LINE>\n" +
                         "<footer>";

    @Test
    void testParseJiraTickets() {
        Collection<String> tickets = AngularContributing.parseJiraTickets("VN-123 TRM-foo TRM-123 TrM-456 vn-456\nvn-66");
        assertThat(tickets, hasItems("VN-123", "TRM-123", "M-456"));
        assertThat(tickets.size(), is(3));
    }

    @Test
    void testSingleLineParsing() {
        String commitMessage = "feat(ci): adding foobar Closes VN-12345";

        AngularParseResult result = AngularContributing.parse(commitMessage);

        assertThat(result.getTickets().contains("VN-12345"), is(true));
        assertThat(result.getType(), is(AngularContributing.Type.feat));
        assertThat(result.getScope(), is("ci"));
        assertThat(result.getSubject(), is("adding foobar Closes VN-12345"));
        assertThat(result.getBody(), is(""));
        assertThat(result.getFooter(), is(""));
    }

    @Test
    void testAngularFormatParsing() {
        String commitMessage = "feat(ci): adding foobar\n\nThis is a major change and is awesome\n\nCloses VN-12345";

        AngularParseResult result = AngularContributing.parse(commitMessage);

        assertThat(result.getTickets().contains("VN-12345"), is(true));
        assertThat(result.getType(), is(AngularContributing.Type.feat));
        assertThat(result.getScope(), is("ci"));
        assertThat(result.getSubject(), is("adding foobar"));
        assertThat(result.getBody(), is("This is a major change and is awesome"));
        assertThat(result.getFooter(), is("Closes VN-12345"));
    }
}
