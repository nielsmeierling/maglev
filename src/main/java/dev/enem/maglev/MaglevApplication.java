package dev.enem.maglev;

import dev.enem.maglev.console.AnsiColorHelper;
import dev.enem.maglev.console.ConsoleMenu;
import dev.enem.maglev.console.ConsoleScreen;
import dev.enem.maglev.console.ConsoleUtil;
import dev.enem.maglev.git.AngularContributing;
import dev.enem.maglev.git.AngularParseResult;
import dev.enem.maglev.git.GitAdapter;
import dev.enem.maglev.git.GitCommit;
import dev.enem.maglev.outputs.ChangelogMarkdown;
import jline.console.ConsoleReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MaglevApplication {

    private static final String MAGLEV_CONF = ".maglev";

    public static void main(String[] args) throws IOException, InterruptedException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        ConsoleScreen console = new ConsoleScreen(reader);

        /*
        * This is needed in order for the arrow keys during a ConsoleMenu selection.
        * It is required that it is initialized this earlier, I am not quite sure why.
        * */
        ConsoleReader cr = new ConsoleReader(); //TODO: needed for arrow keys. better solution? where to put?

        //Enter data using BufferReader
        console.clearScreen();

        /*
        * Check if maglev is configured!
        * */
        Path maglevConf = Paths.get(MAGLEV_CONF);
        if (!Files.exists(maglevConf)) {
            System.out
                .println(AnsiColorHelper.red(String.format("Your project must contain a '%s' file on the project root.",
                                                           MAGLEV_CONF)));
            System.exit(1);
            return;
        }

        String COMMIT_URL = "git_commit_url";
        String ISSUE_URL = "issue_url";

        HashMap<String, String> argParsed = ConsoleScreen.parseSetting(maglevConf, COMMIT_URL, ISSUE_URL);

        console.printf("%s %s=%s%n", AnsiColorHelper.blue(MAGLEV_CONF), COMMIT_URL, argParsed.get(COMMIT_URL));
        console.printf("%s %s=%s%n", AnsiColorHelper.blue(MAGLEV_CONF), ISSUE_URL, argParsed.get(ISSUE_URL));
        //TODO: print maglev version!

        /*
        * Check all preconditions:
        * - is it a git folder?
        * - are we on master?
        * - is master clean?
        * - are we on a tag already?
        * */
        System.out.println("Checking preconditions..");

        File gitDir = new File(".git");
        if (!gitDir.exists()) {
            System.out.println(AnsiColorHelper.red(String.format("No git repository! %s", gitDir.getAbsolutePath())));
            System.exit(1);
            return;
        }

        GitAdapter gitAdapter = new GitAdapter(gitDir.getParentFile());

        String branch = gitAdapter.getCurrentBranch();
        if (!branch.equals("master")) {
            System.out.println(AnsiColorHelper.red(String.format("You must be on a clean master! Current branch is %s",
                                                                 branch)));
            System.exit(1);
            return;
        }

        if (gitAdapter.isOnTagAlready()){
            return;
        }

        console.clearScreen();

        String username = gitAdapter.getUser();

        console.printf("%s: %s%n", AnsiColorHelper.blue("git user"), username);
        console.printf("%s: %s%n", AnsiColorHelper.blue("git branch"), branch);
        console.printf("Working dir: %s%n", new File("").getAbsolutePath());

        if (!gitAdapter.isStatusClean()) {
            return;
        }

        /*
        * User Step 1
        * */
        boolean doCreateRelease = ConsoleUtil.askYesNo(reader, "Create Release?", true);
        if (!doCreateRelease) {
            System.out.println(AnsiColorHelper.green("Ok bye.."));
            return;
        }

        String selectedVersion;
        String lastVersion = null; // '0.0.1'

        List<String> allTags = gitAdapter.getAllTags();

        if (allTags.isEmpty()) {
            //TODO: OR QUIT?
            ConsoleMenu cm = new ConsoleMenu(console,
                                             AnsiColorHelper.yellow("No releases yet (no tags prefixing 'v'), choose starting version:"));

            cm.addAnswer("v0.0.1");
            cm.addAnswer("v0.1.0");
            cm.addAnswer("v1.0.0");

            /*
             * User Step 2a
             * */
            selectedVersion = cm.runSelection(cr);
        } else {
            System.out.printf("Found %d tags.%n", allTags.size());

            allTags.sort(String::compareTo);
            lastVersion = allTags.get(allTags.size() - 1); // 'v0.0.1'
            List<GitCommit> all = gitAdapter.getDiffFromNowTo(lastVersion);

            lastVersion = lastVersion.replace("v", "");

            ArrayList<AngularParseResult> results = AngularContributing.parseNew(all);
            AngularContributing.SuggestResult suggestion = AngularContributing.suggestedBump(lastVersion, results);

            ConsoleMenu cm = new ConsoleMenu(console,
                AnsiColorHelper
                    .yellow(String.format(
                        "lastVersion = %s%nsuggestedVersion = %s%ndue to %s (patch), %s (minor), %s (major) in %s commit subjects",
                        lastVersion, suggestion.suggestedValue, suggestion.patch, suggestion.minor, suggestion.major, suggestion.commits)));

            if (suggestion.suggestedValue != null) {
                cm.addAnswer(suggestion.suggestedValue);
            }

            String[] semVersions = lastVersion.split("\\.");
            cm.addAnswer(String.format("v%d.%d.%d", Integer.valueOf(semVersions[0]), Integer.valueOf(semVersions[1]),
                                       Integer.parseInt(semVersions[2]) + 1));
            cm.addAnswer(
                String.format("v%d.%d.%d", Integer.valueOf(semVersions[0]), Integer.parseInt(semVersions[1]) + 1, 0));
            cm.addAnswer(String.format("v%d.%d.%d", Integer.parseInt(semVersions[0]) + 1, 0, 0));

            /*
             * User Step 2b
             * */
            selectedVersion = cm.runSelection(cr);
        }

        console.printf("Version bump %s -> %s%n", lastVersion, selectedVersion);
        console.render();

        if (lastVersion == null) {//TODO: thiiink!
            System.out.println(AnsiColorHelper.yellow("No lastVersionRef (tag) found, please set a tag like v0.0.1 to collect commit messages from there!"));
            return;
        }

        /*
         * User Step 3
         * */
        boolean doGenerateChangelog = console.askYesNo("Generate Changelog?", true);

        if (doGenerateChangelog) {
            List<GitCommit> all = gitAdapter.getDiffFromNowTo(lastVersion);
            ArrayList<AngularParseResult> results = AngularContributing.parseNew(all);

            String output = new ChangelogMarkdown()
                                .getAsMarkdown(results,
                                               argParsed.get(COMMIT_URL),
                                               argParsed.get(ISSUE_URL));

            output = "# " + selectedVersion + "\n\n" + output;

            Path path = Paths.get("CHANGELOG.md");
            ConsoleScreen.prepend(path, output);

            System.out.printf("%n %s", output);
        }

        /*
         * User Step 4
         * - while askYesNo is to make sure changes are correct
         * - console.printf is called to log it
         * */
        while (!ConsoleUtil.askYesNo(reader, "Please take a look at the changelog. Continue? ", false)) {
            // the 'y' must be given explicitly!
        }
        console.printf("%nPlease take a look at the changelog. Continue? (y/N) y%n");

        console.render();
        gitAdapter.stageFile("CHANGELOG.md");
        console.printf("%s confirmed and staged.%n", AnsiColorHelper.green("CHANGELOG.md"));

        ConsoleScreen.setPropertyToFile("build.gradle", "version ", " '" + selectedVersion + "'");
        gitAdapter.stageFile("build.gradle");
        console.printf("%s bumped and staged.%n", AnsiColorHelper.green("build.gradle"));

        ConsoleScreen.setPropertyToFile("src/main/resources/application.properties", "application.version", selectedVersion);
        gitAdapter.stageFile("src/main/resources/application.properties");

        console.printf("%s bumped and staged.%n", AnsiColorHelper.green("application.properties"));

        /*
         * User Step 5
         * */
        boolean doCommit = console.askYesNo("Do you want to commit those 3 files now?", true);
        if (doCommit) {
            if (!gitAdapter.upToDateWithRemote()) { // check if in the meantime something came up!
                return;
            }

            gitAdapter.commit("chore: update changelog for release " + selectedVersion);
            System.out.println("Committed.");

            gitAdapter.tag(selectedVersion, "");
            System.out.printf("Commit tagged with %s%n", selectedVersion);

            /*
             * User Step 6
             * */
            boolean doPush = console.askYesNo( "Do you want to push now?", true);
            if (doPush) {
                List<String> push = gitAdapter.push();
                for (String p : push) {
                    System.out.println(p);
                }
                System.out.println("Pushed.");
            } else {
                System.out.println(AnsiColorHelper.yellow("Not pushing."));
            }
        } else {
            System.out.println(AnsiColorHelper.yellow("Not committing."));
        }

        //TODO: Announce release?

        System.out.println("Bye!");
        System.exit(0);
    }
}
