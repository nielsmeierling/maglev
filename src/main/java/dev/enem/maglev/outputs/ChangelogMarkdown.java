package dev.enem.maglev.outputs;

import dev.enem.maglev.git.AngularContributing;
import dev.enem.maglev.git.AngularParseResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChangelogMarkdown {


    public String getAsMarkdown(List<AngularParseResult> results, String repoCommitUrl, String jiraBaseUrl) {

        HashMap<AngularContributing.Type, List<AngularParseResult>> byType = new HashMap<>();
        for (AngularParseResult description : results) {
            List<AngularParseResult> orDefault = byType.getOrDefault(description.getType(), new ArrayList<>());

            orDefault.add(description);

            byType.put(description.getType(), orDefault);
        }

        StringBuilder sb = new StringBuilder();

        for (AngularContributing.Type type : byType.keySet()) {

            if (type == null) {
                continue;
            }

            sb.append("### ")
              .append(type.getName())
              .append("\n\n");

            for (AngularParseResult result : byType.get(type)) {
                sb.append("- ");
                if (!result.getScope().isEmpty()) {
                    sb.append("**")
                      .append(result.getScope())
                      .append(":** ");
                }

                sb.append(result.getSubject());

                for (String ticket : result.getTickets()) {
                    sb.append(" [")
                      .append(ticket)
                      .append("](")
                      .append(jiraBaseUrl)
                      .append(ticket)
                      .append(")");
                }

                sb.append(" [∞](").append(repoCommitUrl).append(result.getGitCommit().getId())
                  .append(")\n");
            }

            sb.append("\n\n");
        }

        return sb.toString();
    }
}
