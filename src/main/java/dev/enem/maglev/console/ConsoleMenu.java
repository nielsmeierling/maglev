package dev.enem.maglev.console;

import jline.console.ConsoleReader;

import java.io.IOException;
import java.util.ArrayList;

public class ConsoleMenu {
    private final ConsoleScreen screen;
    private final String question;
    private final ArrayList<String> answers = new ArrayList<>();
    private int pointer = 0;

    public ConsoleMenu(ConsoleScreen screen, String question) {
        this.screen = screen;
        this.question = question;
    }

    public void addAnswer(String answer) {
        this.answers.add(answer);
    }

    public void down() {
        validate();
        if (pointer < answers.size() - 1) {
            pointer++;
        }
    }

    public void up() {
        validate();
        if (pointer != 0) {
            pointer--;
        }
    }

    public String print() {
        StringBuilder sb = new StringBuilder(question);
        sb.append("\n");
        for (int i = 0; i < answers.size(); i++) {
            if (pointer == i) {
                sb.append(AnsiColorHelper.green(answers.get(i)));
            } else {
                sb.append(answers.get(i));
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    private void validate() {
        if (answers.size() == 0) {
            throw new IllegalArgumentException("You need to give at least on answer!");
        }
    }

    public String runSelection(ConsoleReader cr) throws IOException {
        screen.clearScreen();
        System.out.print(this.print());

        while(true) {
            String key = figureKeyPress(cr);

            if (key.equals("ENTER")) {
                break;
            }

            screen.clearScreen();

            if (key.equals("UP")) {
                up();
            }

            if (key.equals("DOWN")) {
                down();
            }

            System.out.print(print());
        }


        return answers.get(pointer);
    }

    public static String figureKeyPress(ConsoleReader cr) throws IOException {
        String key = "";
        int pos = 0;
        while (key.isEmpty()) {
            int pressed = cr.readCharacter();
            if (pos == 0 && pressed == 27 ) {
                pos++;
            }
            if (pos == 1 && pressed == 91) {
                pos++;
            }

            if (pos == 2 && pressed == 65) { // UP
                return "UP";
            }

            if (pos == 2 && pressed == 66) { // DOWN
                return "DOWN";
            }

            if (pressed == 13) {
                return "ENTER";
            }
            //System.out.println("PRESSED " + pressed);
            //System.out.printf("(%d): %d->%s%n", pos, pressed, key);
        }
        return key;
    }
}
