package dev.enem.maglev.console;

import dev.enem.maglev.git.GitAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import java.util.function.Consumer;

public class ConsoleRunner {

    public static List<String> runCmd(File dir, String... command) throws IOException, InterruptedException {

        ProcessBuilder builder = new ProcessBuilder()
                                     .command(command)
                                     .directory(dir);

        Process process = builder.start();

        ArrayList<String> out = new ArrayList<>();
        StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), out::add);
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(streamGobbler);

        int exitCode = process.waitFor();

        // wait for the gobbler!
        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(250));

        if (exitCode != 0) {
            for (String s : out) {
                System.out.println(s);
            }

            throw new IllegalStateException(
                String.format("CMD '%s' exited with %d (%s)", String.join(" ", command), exitCode,
                              dir.getAbsolutePath()));
        }

        executorService.shutdown();

        return out;
    }

    private static class StreamGobbler implements Runnable {
        private final InputStream inputStream;
        private final Consumer<String> consumer;

        public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
            this.inputStream = inputStream;
            this.consumer = consumer;
        }

        @Override
        public void run() {
            new BufferedReader(new InputStreamReader(inputStream)).lines()
                                                                  .forEach(consumer);
        }
    }
}
