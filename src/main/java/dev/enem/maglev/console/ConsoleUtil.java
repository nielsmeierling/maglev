package dev.enem.maglev.console;

import java.io.BufferedReader;
import java.io.IOException;

public class ConsoleUtil {
    // y = 121
    // n = 110
    // enter = 13

    public static boolean askYesNo(BufferedReader reader, String question, boolean defaultAnswer) throws IOException {
        String defaultAnswerLabel = defaultAnswer ? "(Y/n)" : "(y/N)";

        System.out.printf("%s %s", question, defaultAnswerLabel);

        while (true) {
            int read = reader.read();

            if (read == 121) {
                System.out.printf(" %s ", (char) read);
                return true;
            }

            if (read == 110) {
                System.out.printf(" %s ", (char) read);
                return false;
            }

            if (read == 13) {
                System.out.printf(" %s ", defaultAnswer ? "y" : "n");
                return defaultAnswer;
            }
        }
    }
}
