package dev.enem.maglev.console;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/*
* Use this class to render global state.
* After eg. a selection, use render() to get to that state again.
* */
public class ConsoleScreen {

    private final BufferedReader reader;
    private final StringBuilder screen = new StringBuilder();

    public ConsoleScreen(BufferedReader reader) {
        this.reader = reader;
    }

    public static void prepend(Path path, String output) throws IOException {
        if (!Files.exists(path)) {
            Files.write(path, "".getBytes());
        }
        byte[] oldContent = Files.readAllBytes(path);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(output.getBytes());
        outputStream.write(oldContent);

        byte[] c = outputStream.toByteArray();

        Files.write(path, c);

    }

    public void printf(String format, Object... args) {
        String line = String.format(format, args);
        screen.append(line);
        System.out.print(line);
    }

    public void render() {
        clearScreen();
        System.out.print(screen.toString());
    }

    public void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public boolean askYesNo(String question, boolean defaultAnswer) throws IOException {
        boolean answer = ConsoleUtil.askYesNo(reader, question, defaultAnswer);
        screen.append(String.format("%s %s", question, answer ? " y" : " n"));
        return answer;
    }

    /*
    * Can parse property files in the format of
    * key=value
    * */
    public static HashMap<String, String> parseSetting(Path file, String... keys) throws IOException {
        HashMap<String, String> out = new HashMap<>();

        Files.readAllLines(file).forEach(line -> {
            for (String key : keys) {
                if (line.startsWith(key)) {
                       out.put(key, line.split("=")[1].trim().replace("'", ""));
                }
            }
        });

        return out;
    }

    /*
    * Replaces all appearances of
    * key=
    * or appends it as last value if not yet present.
    * */
    public static void setPropertyToFile(String file, String key, String value) throws IOException {
        StringBuilder sb = new StringBuilder();

        AtomicBoolean found = new AtomicBoolean(false);
        Files.readAllLines(Paths.get(file)).forEach(line -> {
            if (line.split("=")[0].equals(key)) {
                found.set(true);
                sb.append(key).append("=").append(value);
            } else {
                sb.append(line);
            }
            sb.append("\n");
        });

        if (!found.get()) {
            sb.append(key).append("=").append(value);
        }

        Files.write(Paths.get(file), sb.toString().getBytes()); //TODOOOO
    }
}
