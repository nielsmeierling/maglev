package dev.enem.maglev.git;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class AngularParseResult {

    private AngularContributing.Type type;
    private String scope = "";
    private String subject = "";

    private String body = "";
    private String footer = "";

    private HashSet<String> tickets = new HashSet<>();

    private List<String> notParsables = new ArrayList<>();

    //new one
    private GitCommit gitCommit;

    public void setType(AngularContributing.Type type) {
        this.type = type;
    }

    public AngularContributing.Type getType() {
        return type;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public HashSet<String> getTickets() {
        return tickets;
    }

    public List<String> getNotParsables() {
        return notParsables;
    }

    public void setNotParsables(List<String> notParsables) {
        this.notParsables = notParsables;
    }

    public GitCommit getGitCommit() {
        return gitCommit;
    }

    public void setGitCommit(GitCommit gitCommit) {
        this.gitCommit = gitCommit;
    }
}
