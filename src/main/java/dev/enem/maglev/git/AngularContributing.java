package dev.enem.maglev.git;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// https://github.com/angular/angular/blob/master/CONTRIBUTING.md#type
// https://help.github.com/en/github/managing-your-work-on-github/closing-issues-using-keywords

public class AngularContributing {

    public enum Type {
        build ("Build Changes")
        , ci("Continuous Integration")
        , docs("Documentation")
        , feat ("Features")
        , fix ("Bug Fixes")
        , perf ("Performance Improvements")
        , refactor("Code Refactoring")
        , style("Styles")
        , test("Tests")
        // custom types from us:
        , revert ("Reverts")
        , chore("Chores");

        private final String name;

        Type(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

    }

    public static ArrayList<AngularParseResult> parseNew(List<GitCommit> all) {
        ArrayList<AngularParseResult> out = new ArrayList<>();

        for (GitCommit gitCommit : all) {
            AngularParseResult result = parseSubject(gitCommit.getSubject()); //+ type&scope
            result.setGitCommit(gitCommit);
            result.setBody(gitCommit.getBody());

            result.getTickets().addAll(parseJiraTickets(gitCommit.getSubject()));
            result.getTickets().addAll(parseJiraTickets(gitCommit.getBody()));

            out.add(result);
        }

        return out;
    }

    public static AngularParseResult parse(String commitMessage) {
        String[] lines = commitMessage.split("\\n\\n");

        AngularParseResult angularParseResult = parseSubject(lines[0]);


        angularParseResult.getTickets().addAll(parseJiraTickets(commitMessage));


        if (lines.length > 1) {
            angularParseResult.setBody(lines[1]);
        }

        if (lines.length > 2) {
            angularParseResult.setFooter(lines[2]);
        }

        if (lines.length > 3) {
            System.out.println("NOT PARSABLE!!!"); //TODO: think about this!!
        }

        return angularParseResult;
    }

    public static AngularParseResult parseSubject(String line) {
        AngularParseResult out = new AngularParseResult();

        int i = line.indexOf(":");
        if (i == -1) {
            out.getNotParsables().add(line);
            return out;
        }

        String firstPart = line.substring(0, i);
        String[] parts = firstPart.split("[\\(\\)]");
        if (parts.length == 2) {
            for (Type value : Type.values()) {
                if (parts[0].toLowerCase().startsWith(value.name().toLowerCase())) {

                    out.setType(value);
                    out.setScope(parts[1]);

                    String subject = line.substring(i + 1);
                    out.setSubject(subject.trim());

                    break;
                }
            }
        }
        return out;
    }

    static final Pattern JIRA_PATTERN = Pattern.compile("((?<!([A-Z]{1,10})-?)[A-Z]+-\\d+)");
    public static Collection<String> parseJiraTickets(String line) {
        ArrayList<String> out = new ArrayList<>();
        Matcher m = JIRA_PATTERN.matcher(line);

        while (m.find()) {
            out.add(m.group(1));
        }

        return out;
    }

    public static SuggestResult suggestedBump(String lastVersion, ArrayList<AngularParseResult> resultList) {
        SuggestResult out = new SuggestResult();
        out.commits = resultList.size();
        out.suggestedValue= null;

        String[] semVersions = lastVersion.split("\\.");
        int weight = 0;
        for (AngularParseResult angularParseResult : resultList) {
            if (angularParseResult.getSubject().contains("(major)")) {
                out.major++;
                out.suggestedValue = String.format("v%d.%d.%d", Integer.parseInt(semVersions[0]) + 1, 0, 0);
                weight = 100;
            }

            if (angularParseResult.getSubject().contains("(minor)")) {
                out.minor++;
                if (weight < 100) {
                    out.suggestedValue = String
                                 .format("v%d.%d.%d", Integer.valueOf(semVersions[0]),
                                         Integer.parseInt(semVersions[1]) + 1, 0);
                    weight = 10;
                }
            }
            if (angularParseResult.getSubject().contains("(patch)")) {
                out.patch++;
                if (weight < 10) {
                    out.suggestedValue = String
                                 .format("v%d.%d.%d", Integer.valueOf(semVersions[0]), Integer.valueOf(semVersions[1]),
                                         Integer.parseInt(semVersions[2]) + 1);
                }

            }
        }
        return out;
    }

    public static class SuggestResult {
        public String suggestedValue;
        public int patch = 0;
        public int minor = 0;
        public int major = 0;
        public int commits = 0;
    }

}
