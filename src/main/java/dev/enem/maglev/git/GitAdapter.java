package dev.enem.maglev.git;

import dev.enem.maglev.console.AnsiColorHelper;
import dev.enem.maglev.console.ConsoleRunner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GitAdapter {
    private final File projectDir;

    public GitAdapter(File projectDir) {
        this.projectDir = projectDir;
    }

    public List<String> status() throws IOException, InterruptedException {
        return runCmd(projectDir, "git", "status");
    }

    public List<String> getAllTags() throws IOException, InterruptedException {
        return runCmd(projectDir, "git", "tag", "-l", "v*");
    }

    public void stageFile(String filepath) throws IOException, InterruptedException {
        runCmd(projectDir, "git", "add", filepath);
    }

    public void commit(String message) throws IOException, InterruptedException {
        runCmd(projectDir, "git", "commit", "-m", message); //TODO: escape " or not?
    }

    public void tag(String selectedVersion, String tagMessage) throws IOException, InterruptedException {
        //git tag -a v1.4 -m "my version 1.4"
        runCmd(projectDir, "git", "tag", "-a", selectedVersion, "-m",
               "\"" + tagMessage + "\"");
    }

    public List<String> push() throws IOException, InterruptedException {
        return runCmd(projectDir, "git", "push");
    }

    public String getUser() throws IOException, InterruptedException {
        List<String> username = runCmd(projectDir, "git", "config", "user.name");
        List<String> email = runCmd(projectDir, "git", "config", "user.email");
        return String.format("%s <%s>", username.get(0), email.get(0));
    }

    public boolean isOnTagAlready() throws IOException, InterruptedException {
        List<String> strings = runCmd(projectDir, "git", "tag", "--points-at", "HEAD");
        if (!strings.isEmpty()) {
            System.out.println(AnsiColorHelper.red("Already on a tag: " + strings.get(0)));
            return true;
        }
        return false;
    }

    public String getCurrentBranch() throws IOException, InterruptedException {
        List<String> strings = runCmd(projectDir, "git", "rev-parse", "--abbrev-ref", "HEAD");
        if (strings.size() != 1) {
            StringBuilder sb = new StringBuilder();
            sb.append("S[").append("\n");
            for (String string : strings) {
                sb.append(string).append("\n");
            }
            sb.append("]");
            throw new IllegalStateException(
                String.format("Cannot determine branch!. Responded with:%n #####%n%s%n #####", sb.toString()));
        }
        return strings.get(0);
    }

    public boolean isStatusClean() throws IOException, InterruptedException {
        //$ git status
        //On branch master
        //Your branch is ahead of 'origin/master' by 3 commits.
        //  (use "git push" to publish your local commits)
        //
        //nothing to commit, working tree clean

        List<String> output = status();

        int matched = 0;
        for (String s : output) {
            if (s.contains("Your branch is up to date with 'origin/master'.")) {
                matched++;
            }
            if (s.contains("nothing to commit, working tree clean")) {
                matched++;
            }
        }

        if (matched == 2) { //both states need to apply!
            return true;
        }

        System.out.println("'$ git status' returned:");
        System.out.println();
        for (String o : output) {
            System.out.println(o);
        }
        System.out.println();
        System.out.println(AnsiColorHelper.yellow(
            "There are either a) changes to files that are not committed or b) origin/master has newer commits! Please fix first."));
        return false;
    }



    public List<GitCommit> getDiffFromNowTo(String lastVersion) throws IOException, InterruptedException {
        if (!lastVersion.startsWith("v")) {
            lastVersion = "v" + lastVersion;
        }

        List<String> rawResult = runCmd(projectDir, "git", "--no-pager", "log",
                                      "--pretty=format:maglevSEP %H%nmaglevTime %ct%nmaglevSubject %smaglevBody%bmaglevEnd", "..." + lastVersion);

        ArrayList<GitCommit> all = new ArrayList<>();
        GitCommit gitCommit = null;

       /* for (String raw : rawResult) {
            System.out.println(raw);
        }*/

       /*
       * one-liner looks like this:
       * maglevSEP 0385a68ccb37978ec87af57c08db09506f38f601
       * maglevTime 1584552097
       * maglevSubject dummy commitmaglevBodymaglevEn
       * */

        for (String rawLine : rawResult) {
            if (rawLine.startsWith("maglevSEP ")) {
                if (gitCommit != null) {
                    all.add(gitCommit);
                }

                gitCommit = new GitCommit(rawLine.replace("maglevSEP ", ""));
            } else if (rawLine.startsWith("maglevTime ")) {
                gitCommit.setTime(Long.parseLong(rawLine.replace("maglevTime ", "")));
            } else if (rawLine.startsWith("maglevSubject ")) {
                gitCommit.setSubject(rawLine.replace("maglevSubject ", "")
                                            .replace("maglevBodymaglevEnd", "")); //in case of one liner!

                if (rawLine.contains("maglevBodymaglevEnd")) {
                    all.add(gitCommit);
                    gitCommit = null;
                }
            } else if (rawLine.startsWith("maglevBody ")) {
                gitCommit.setBody(rawLine.replace("maglevBody ", ""));
            } else if (rawLine.contains("maglevEnd")) { // dont miss last or only one!
                if (gitCommit != null) { //waaaah
                    all.add(gitCommit);
                    gitCommit = null;
                }
            } else {
                gitCommit.setBody(gitCommit.getBody() + "\n" + rawLine);
            }
        }

        return all;
    }

    public boolean upToDateWithRemote() throws IOException, InterruptedException {
        List<String> output = status();

        for (String s : output) {
            if (s.contains("Your branch is up to date with 'origin/master'.")) {
                return true;
            }
        }

        System.out.println("'$ git status' returned:");
        System.out.println();
        for (String s : output) {
            System.out.println(s);
        }
        System.out.println();

        return false;
    }

    private List<String> runCmd(File dir, String... command) throws IOException, InterruptedException {
        return ConsoleRunner.runCmd(dir, command);
    }
}
