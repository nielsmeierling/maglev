package dev.enem.maglev.git;

public class GitCommit {

    private final String id;
    private Long time;
    private String subject = "";
    private String body = "";

    public GitCommit(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
