# v1.1.3

### Bug Fixes

- **git:** fixing npe [∞](https://gitlab.com/nielsmeierling/maglev/commits/725927b38f53f6db48850f296670eba089d83e4a)


### Code Refactoring

- **general:** refactored according to reusability and added some comments [∞](https://gitlab.com/nielsmeierling/maglev/commits/4a9378a02beabc7978ca3d84b6bf403af20d6643)


# v0.0.2

### Bug Fixes

- **git:** fixing npe [∞](https://gitlab.com/nielsmeierling/maglev/commits/725927b38f53f6db48850f296670eba089d83e4a)


### Code Refactoring

- **general:** refactored according to reusability and added some comments [∞](https://gitlab.com/nielsmeierling/maglev/commits/4a9378a02beabc7978ca3d84b6bf403af20d6643)


# v1.1.2

### Code Refactoring

- **general:** worked on intellij recommendations [∞](https://gitlab.com/nielsmeierling/maglev/commits/21f5bdc3c509b02eda6339a1715e10ed33eb0661)
- **console:** replacing redundant printf() [∞](https://gitlab.com/nielsmeierling/maglev/commits/b89f79deff975dd6d99e08aabfb5e1ae336d8760)


# v1.1.1

### Bug Fixes

- **git:** now parsing even if only 1 commit with only subject line (has been ignored before) [∞](https://gitlab.com/nielsmeierling/maglev/commits/4a5de4c177af6db547859f523f36061fdb872a07)


### Code Refactoring

- **maglev:** commented some code and beautified [∞](https://gitlab.com/nielsmeierling/maglev/commits/6139e942efe061b2333e4489a7dc340277f13a00)


### Features

- **git:** removing all nayuki helper and using bash for all git related things [∞](https://gitlab.com/nielsmeierling/maglev/commits/566c7c2b1b00f7780161b1797f09ace8624d92e0)


# v1.1.0

### Code Refactoring

- **git:** removed commented code [∞](https://gitlab.com/nielsmeierling/maglev/commits/6d26788b36b008a2fcee4c4bf43317faa5119847)


### Chores

- **readme:** updated readme [∞](https://gitlab.com/nielsmeierling/maglev/commits/660e988a59fbcdbe20dc11eacf5f179a257b3234)


### Bug Fixes

- **git:** just check for remote changes the 2nd time [∞](https://gitlab.com/nielsmeierling/maglev/commits/c6f5bba0415c9fbbdaceae5f8af1919451bb02cc)
- **git:** explore all parent trees and filter by commitTime (not the endId!) [∞](https://gitlab.com/nielsmeierling/maglev/commits/500d8d987f1c1df142af420a6462b98683064581)
- **git:** recursively walk through all parents and throw away all, where the endId (latest tag) is not found [∞](https://gitlab.com/nielsmeierling/maglev/commits/3783db77a1ca883254d64ea02ed55f7ed0a88985)
- **git:** using gitlab via bash commands and fixed outputs [∞](https://gitlab.com/nielsmeierling/maglev/commits/79bbaafee0c0fe22730cc014f0b9ecc2d0fe7f44)



# v1.2.0

### Features

- **console-screen:** adding console util to ask precise yes/no question [VN-189](https://jira.de/browse/VN-189) [∞](gitlab.de/vertical/commit/7722dda)
- **console-screen:** adding stateful console-screen [VN-1337](https://jira.de/browse/VN-1337) [VN-9001](https://jira.de/browse/VN-9001) [∞](gitlab.de/vertical/commit/a2f01a4)


# v1.1.0

# v1.1.0

# v1.1.0

# v1.1.0

# v1.1.0

# v1.0.6

### Features

- **changelog:** adding commit and push [∞](gitlab.de/vertical/commit/26ed14b)


# v1.0.5

### Bug Fixes

- **test:** fixing se tests [VN-12345](https://jira.de/browse/VN-12345) [∞](gitlab.de/vertical/commit/f3f53c8)


### Code Refactoring

- **changelog-format:** refactoring the way the changelog is rendered [∞](gitlab.de/vertical/commit/8aec914)
- **contribute-type:** adding contribute types according to angular setup [VN-666](https://jira.de/browse/VN-666) [∞](gitlab.de/vertical/commit/1e08b00)


### Tests

- **changelog:** adding better output for CHANGELOG.md [∞](gitlab.de/vertical/commit/5fed78c)


### Documentation

- **readme:** updating types of readme [∞](gitlab.de/vertical/commit/c2a05f2)


# v1.0.5

### Bug Fixes

- **test:** fixing se tests [VN-12345](https://jira.de/browse/VN-12345) [∞](gitlab.de/vertical/commit/f3f53c8)


### Code Refactoring

- **changelog-format:** refactoring the way the changelog is rendered [∞](gitlab.de/vertical/commit/8aec914)
- **contribute-type:** adding contribute types according to angular setup [VN-666](https://jira.de/browse/VN-666) [∞](gitlab.de/vertical/commit/1e08b00)


### Tests

- **changelog:** adding better output for CHANGELOG.md [∞](gitlab.de/vertical/commit/5fed78c)


### Documentation

- **readme:** updating types of readme [∞](gitlab.de/vertical/commit/c2a05f2)


# v1.0.5

### Bug Fixes

- **test:** fixing se tests [VN-12345](https://jira.de/browse/VN-12345) [∞](gitlab.de/vertical/commit/f3f53c8)


### Code Refactoring

- **changelog-format:** refactoring the way the changelog is rendered [∞](gitlab.de/vertical/commit/8aec914)
- **contribute-type:** adding contribute types according to angular setup [VN-666](https://jira.de/browse/VN-666) [∞](gitlab.de/vertical/commit/1e08b00)


### Tests

- **changelog:** adding better output for CHANGELOG.md [∞](gitlab.de/vertical/commit/5fed78c)


### Documentation

- **readme:** updating types of readme [∞](gitlab.de/vertical/commit/c2a05f2)


