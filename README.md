# Build as Jar
     gradle shadowJar

# Run jar
    java -jar build/libs/maglev.jar
# Use alias
    alias maglev="./gradlew shadowJar && java -jar $(pwd)/build/libs/maglev.jar"
    alias maglev="gradle clean shadowJar && java -jar $(pwd)/build/libs/maglev.jar"
    alias maglev="java -jar $(pwd)/build/libs/maglev.jar"
    alias maglev="java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=1044 -jar $(pwd)/build/libs/maglev.jar"

# Create native-image

    native-image --verbose -jar build/libs/maglev.jar --no-fallback -H:+TraceClassInitialization -H:+ReportExceptionStackTraces -H:IncludeResourceBundles=jline.console.completer.CandidateListCompletionHandler

# Conventions

## Git commit message conventions:

Basically the format has been copied from here:

    https://github.com/angular/angular/blob/master/CONTRIBUTING.md#commit-message-format 

BUT i adapted it, since git does not support the notion of a "footer", so the format looks like:

```
<type>(<scope>): <subject>
<body>
```
 

### Supported Types

https://github.com/angular/angular/blob/master/CONTRIBUTING.md#type

|Type|Name|
|----|----|
|`build`|Build Changes|
|`ci`|Continuous Integration|
|`docs`|Documentation|
|`feat`|Features|
|`fix`|Bug Fixes|
|`perf`|Performance Improvements|
|`refactor`|Code Refactoring|
|`style`|Styles|
|`test`|Tests|

Additionally, I added those types:

|Type|Name|
|----|----|
|`revert`|Reverts|
|`chore`|Chores|


//TODO: make types to appear in changelog configurable!! (and which depth: subject, body, footer)

### Automated version detection

Include one of those values in the commit `subject` to propose a version bump:

`(patch)`
`(minor)`
`(major)`

### Automated jira issue detection

If you mention tickets in the jira format , eg.

```
((?<!([A-Z]{1,10})-?)[A-Z]+-\d+)
```

in a commit message (no matter where), they will all be appended to the subject line in the changelog.

### Result as markdown

```
# <version, eg. 'v0.4.0'>

### <type>

- **<scope>:** <subject> [<jira-ticket-id>](<issue_url>/<jira-ticket-id>) [∞](<git_commit_url>/<commit-sha>)
```


### Maybe also consider

// https://help.github.com/en/github/managing-your-work-on-github/closing-issues-using-keywords
